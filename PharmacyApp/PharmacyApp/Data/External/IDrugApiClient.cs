﻿using PharmacyApp.Data.Dto;
using PharmacyApp.Data.Dto.SuklApi.RequestFilter;


namespace PharmacyApp.Data.External
{
    public interface IDrugApiClient
    {
        Task<DrugPageDto?> GetDrugPage(int page = 0, int size = 10);
        Task<DrugPageDto?> GetDrugPageByName(string name, int page = 0, int size = 10);
        Task<DrugDto?> GetFirstDrugByName(string name);
        Task<DrugDto?> GetDrugBySuklCode(string suklCode);
    }
}
