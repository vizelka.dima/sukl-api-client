﻿using PharmacyApp.Data.Dto.SuklApi.RequestQuery;

namespace PharmacyApp.Data.Dto.SuklApi.RequestFilter
{
    public class DrugPageFilter : PageFilter<DrugFieldFilter, DrugWhereFilter>{}
}
