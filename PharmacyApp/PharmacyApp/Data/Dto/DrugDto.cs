﻿namespace PharmacyApp.Data.Dto
{
    public class DrugDto
    {
        public string Name { get; set; } = default!;
        public string SuklCode { get; set; } = default!;

    }
}
