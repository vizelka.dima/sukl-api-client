﻿namespace PharmacyApp.Data.Dto
{
    public class BasePageDto<T>
        where T : class
    {
        public List<T>? Data { get; set; } = new();
    }
}
